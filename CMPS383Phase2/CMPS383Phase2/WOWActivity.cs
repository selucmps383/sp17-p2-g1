﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using System.IO;
using System.Net;
using System.Security;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WOWSharp.Community;
using WOWSharp.Community.Diablo;
using WOWSharp.Community.Wow;

using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CMPS383Phase2
{
	[Activity(Label = "WOWActivity")]
	public class WOWActivity : Activity
	{

        private TextView txtWoWName, txtRealm;
        private EditText editName, editRealm;
        private Button btnWoWSearch, btnClear;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.WorldOfWarcraft);

            txtWoWName = FindViewById<TextView>(Resource.Id.txtWoWName);
            txtRealm = FindViewById<TextView>(Resource.Id.txtRealm);
            editName = FindViewById<EditText>(Resource.Id.editName);
            editRealm = FindViewById<EditText>(Resource.Id.editRealm);
            btnWoWSearch = FindViewById<Button>(Resource.Id.btnWoWSearch);
            btnClear = FindViewById<Button>(Resource.Id.btnClear);
            btnWoWSearch.Click += btnWoWSearchClick;
            btnClear.Click += btnClearClick;


            WOWSharp.Community.ApiClient.TestMode = true;
			//WOWSharp.Community.ApiClient.TestMode = true;

			//TestWowClientAsync().Wait();


			// Create your application here
		}

        void btnClearClick(object sender, EventArgs e)
        {
            editName.Text = "";
            editRealm.Text = "";
        }
        void btnWoWSearchClick(object sender, EventArgs e)
        {
            string privateKey = "r8qtcbzwx8sc3dvrxwsgbh4rczmkf8jm";
            string publicKey = "7YHjK9FPCQZmp424wAgdB8Ca82YevVVU";
            var pair = new ApiKeyPair(publicKey, privateKey);
            var client = new WowClient(Region.US, pair, "en-us", null);
            try
            {
                var charName = editName.Text;
                var realmName = editRealm.Text;
                var character = client.GetCharacterAsync(charName, realmName, CharacterFields.All).Result;
                var x = character.Class;
            }
            catch(Exception){}
            

            //@Matt: Decide how you will display your info and add in the necessary textviews and such
        }

        private static void TestWoWClient()
		{
			string privateKey = "r8qtcbzwx8sc3dvrxwsgbh4rczmkf8jm";    //ConfigurationManager.AppSettings["PrivateKey"];
			string publicKey = "7YHjK9FPCQZmp424wAgdB8Ca82YevVVU";    //ConfigurationManager.AppSettings["PublicKey"];
			var pair = new ApiKeyPair(publicKey, privateKey);

			// Init client
			var client = new WowClient(Region.US, pair, "en-us", null);

			// Get character data
			var character = client.GetCharacterAsync("Zantlock", "Thrall", CharacterFields.All).Result;
			// Refresh data
			character.RefreshAsync(client).Wait();

		}

		private async static Task TestWowClientAsync()
		{
			string privateKey = "r8qtcbzwx8sc3dvrxwsgbh4rczmkf8jm";    //ConfigurationManager.AppSettings["PrivateKey"];
			string publicKey = "7YHjK9FPCQZmp424wAgdB8Ca82YevVVU";    //ConfigurationManager.AppSettings["PublicKey"];
			var pair = new ApiKeyPair(publicKey, privateKey);
            Character character;
			// Init client
			var client = new WowClient(Region.US, pair, "en-us", null);

			// Get character data
            character = client.GetCharacterAsync("Zantlock", "Thrall", CharacterFields.All).Result;
			
			// Refresh data
			await character.RefreshAsync(client);
		}
	}
}
