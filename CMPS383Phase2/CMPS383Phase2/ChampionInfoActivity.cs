using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using RiotSharp;

namespace CMPS383Phase2
{
    [Activity(Label = "CMPS383Phase2", Icon = "@drawable/leagueicon",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class ChampionInfoActivity : Activity
    {
        private EditText championName;
        private Button btnSearchChampStats;
        private TextView txtAverageAssists;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ChampionInfo);
            championName = FindViewById<EditText>(Resource.Id.championName);
            btnSearchChampStats = FindViewById<Button>(Resource.Id.btnSearchChampStats);
            txtAverageAssists = FindViewById<TextView>(Resource.Id.txtAverageAssists);
            
            string summonerName = Intent.GetStringExtra("summonerName") ?? "Summoner name not provided";
            var api = RiotApi.GetInstance("RGAPI-249041df-8508-449a-81b1-ae2e9f820ed1");
            var region = (Region)Enum.Parse(typeof(Region), "na");
            try
            {
                var summoner = api.GetSummoner(region, summonerName);
                var rankedSummary = summoner.GetStatsRanked(RiotSharp.StatsEndpoint.Season.Season2016).FirstOrDefault();
                txtAverageAssists.Text = rankedSummary.Stats.AverageAssists.ToString();
            }
            catch (Exception) { }

        }

    }
}