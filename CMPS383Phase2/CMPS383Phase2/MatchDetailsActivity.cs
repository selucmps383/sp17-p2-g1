using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Content.PM;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using RiotSharp.GameEndpoint;
using Android.Graphics;
using RiotSharp;
using System.Text.RegularExpressions;

namespace CMPS383Phase2
{
    [Activity(Label = "Phase 2", Icon = "@drawable/leagueicon",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class MatchDetailsActivity : Activity
    {
        //I am aware I could use arrays for some of these variables...don't judge - JM
        private TextView finalResult;
        private TextView champLevel;
        private TextView KDAText;
        private TextView overallKDA;
        private TextView txtBuild;
        private double kills;
        private double deaths;
        private double assists;
        private ImageView item0Pic, item1Pic, item2Pic, item3Pic, item4Pic, item5Pic, itemTrinket;
        private ImageView champPic;
        private TextView txtItem0, txtItem1, txtItem2, txtItem3, txtItem4, txtItem5, txtTrinket;
        private TextView txtChampName;
        private TextView txtSummonerSpell, txtSpell1, txtSpell2;
        private ImageView imgSpell1, imgSpell2;
        private TextView txtSpacing1, txtSpacing2, txtSpacing3, txtSpacing4, txtSpacing5, txtSpacing6, txtSpacing7;
        private Button btnName1, btnName2, btnName3, btnName4, btnName5, btnName6, btnName7, btnName8, btnName9;
        private long otherId1, otherId2, otherId3, otherId4, otherId5, otherId6, otherId7, otherId8, otherId9;
        private RiotApi api;
        private StaticRiotApi info;
        private RiotSharp.Region region;
        private long matchId;
        private TextView txtOtherPlayers;
        private TextView txtSummonerName;
        private string summonerName;
        private bool history = false;
        private Button btnBack;
        private ImageView imgWinState;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MatchDetails3);

            finalResult = FindViewById<TextView>(Resource.Id.txtFinalResult);
            champLevel = FindViewById<TextView>(Resource.Id.txtChampLevel);
            KDAText = FindViewById<TextView>(Resource.Id.txtKDAText);
            overallKDA = FindViewById<TextView>(Resource.Id.txtOverallKDAText);
            txtBuild = FindViewById<TextView>(Resource.Id.txtBuild);
            txtOtherPlayers = FindViewById<TextView>(Resource.Id.txtOtherPlayers);

            item0Pic = FindViewById<ImageView>(Resource.Id.imgItem0);
            item1Pic = FindViewById<ImageView>(Resource.Id.imgItem1);
            item2Pic = FindViewById<ImageView>(Resource.Id.imgItem2);
            item3Pic = FindViewById<ImageView>(Resource.Id.imgItem3);
            item4Pic = FindViewById<ImageView>(Resource.Id.imgItem4);
            item5Pic = FindViewById<ImageView>(Resource.Id.imgItem5);
            itemTrinket = FindViewById<ImageView>(Resource.Id.imgTrinket);
            

            txtSummonerSpell = FindViewById<TextView>(Resource.Id.txtSummonerSpell);
            txtSpell1 = FindViewById<TextView>(Resource.Id.txtSpell1);
            txtSpell2 = FindViewById<TextView>(Resource.Id.txtSpell2);
            imgSpell1 = FindViewById<ImageView>(Resource.Id.imgSpell1);
            imgSpell2 = FindViewById<ImageView>(Resource.Id.imgSpell2);

            champPic = FindViewById<ImageView>(Resource.Id.imgChampion);
            txtChampName = FindViewById<TextView>(Resource.Id.txtChampName);

            txtItem0 = FindViewById<TextView>(Resource.Id.txtItem0);
            txtItem1 = FindViewById<TextView>(Resource.Id.txtItem1);
            txtItem2 = FindViewById<TextView>(Resource.Id.txtItem2);
            txtItem3 = FindViewById<TextView>(Resource.Id.txtItem3);
            txtItem4 = FindViewById<TextView>(Resource.Id.txtItem4);
            txtItem5 = FindViewById<TextView>(Resource.Id.txtItem5);
            txtTrinket = FindViewById<TextView>(Resource.Id.txtTrinket);

            txtSpacing1 = FindViewById<TextView>(Resource.Id.txtSpacing1);
            txtSpacing2 = FindViewById<TextView>(Resource.Id.txtSpacing2);
            txtSpacing3 = FindViewById<TextView>(Resource.Id.txtSpacing3);
            txtSpacing4 = FindViewById<TextView>(Resource.Id.txtSpacing4);
            txtSpacing5 = FindViewById<TextView>(Resource.Id.txtSpacing5);
            txtSpacing6 = FindViewById<TextView>(Resource.Id.txtSpacing6);
            txtSpacing7 = FindViewById<TextView>(Resource.Id.txtSpacing7);

            btnName1 = FindViewById<Button>(Resource.Id.btnName1);
            btnName2 = FindViewById<Button>(Resource.Id.btnName2);
            btnName3 = FindViewById<Button>(Resource.Id.btnName3);
            btnName4 = FindViewById<Button>(Resource.Id.btnName4);
            btnName5 = FindViewById<Button>(Resource.Id.btnName5);
            btnName6 = FindViewById<Button>(Resource.Id.btnName6);
            btnName7 = FindViewById<Button>(Resource.Id.btnName7);
            btnName8 = FindViewById<Button>(Resource.Id.btnName8);
            btnName9 = FindViewById<Button>(Resource.Id.btnName9);

            btnName1.Click += btnName1Click;
            btnName2.Click += btnName2Click;
            btnName3.Click += btnName3Click;
            btnName4.Click += btnName4Click;
            btnName5.Click += btnName5Click;
            btnName6.Click += btnName6Click;
            btnName7.Click += btnName7Click;
            btnName8.Click += btnName8Click;
            btnName9.Click += btnName9Click;

            txtSummonerName = FindViewById<TextView>(Resource.Id.txtSummonerName);
            summonerName = Intent.GetStringExtra("summonerName");

            btnBack = FindViewById<Button>(Resource.Id.btnBack);
            btnBack.Click += btnBackClick;

            imgWinState = FindViewById<ImageView>(Resource.Id.imgWinState);


            var doc = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var path = System.IO.Path.Combine(doc, "matchDetails.xml");
            try
            {
                Game currentMatch = DeSerializeObject<Game>(path);
                
                api = RiotApi.GetInstance("RGAPI-249041df-8508-449a-81b1-ae2e9f820ed1");
                info = StaticRiotApi.GetInstance("RGAPI-249041df-8508-449a-81b1-ae2e9f820ed1");
                region = (RiotSharp.Region)Enum.Parse(typeof(RiotSharp.Region), "na");
                matchId = currentMatch.GameId;

                //try/catching because I want to be able to show bot games
                try
                {
                    //set the button text and ID's for the "Other Player" section to other player names/ID's
                    //as well as set the text color for which team they were on
                    btnName1.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[0].SummonerId).Name;
                    otherId1 = currentMatch.FellowPlayers[0].SummonerId;
                    if (currentMatch.FellowPlayers[0].TeamId == 100)
                    {
                        btnName1.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName1.SetTextColor(Color.Red);
                    }

                    //Only show the "Other Players: " text if there is at least one non-bot player
                    txtOtherPlayers.Visibility = Android.Views.ViewStates.Visible;

                    btnName2.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[1].SummonerId).Name;
                    otherId2 = currentMatch.FellowPlayers[1].SummonerId;
                    if(currentMatch.FellowPlayers[1].TeamId == 100)
                    {
                        btnName2.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName2.SetTextColor(Color.Red);
                    }

                    btnName3.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[2].SummonerId).Name;
                    otherId3 = currentMatch.FellowPlayers[2].SummonerId;
                    if (currentMatch.FellowPlayers[2].TeamId == 100)
                    {
                        btnName3.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName3.SetTextColor(Color.Red);
                    }

                    btnName4.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[3].SummonerId).Name;
                    otherId4 = currentMatch.FellowPlayers[3].SummonerId;
                    if (currentMatch.FellowPlayers[3].TeamId == 100)
                    {
                        btnName4.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName4.SetTextColor(Color.Red);
                    }

                    btnName5.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[4].SummonerId).Name;
                    otherId5 = currentMatch.FellowPlayers[4].SummonerId;
                    if (currentMatch.FellowPlayers[4].TeamId == 100)
                    {
                        btnName5.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName5.SetTextColor(Color.Red);
                    }

                    btnName6.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[5].SummonerId).Name;
                    otherId6 = currentMatch.FellowPlayers[5].SummonerId;
                    if (currentMatch.FellowPlayers[5].TeamId == 100)
                    {
                        btnName6.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName6.SetTextColor(Color.Red);
                    }

                    btnName7.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[6].SummonerId).Name;
                    otherId7 = currentMatch.FellowPlayers[6].SummonerId;
                    if (currentMatch.FellowPlayers[6].TeamId == 100)
                    {
                        btnName7.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName7.SetTextColor(Color.Red);
                    }

                    btnName8.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[7].SummonerId).Name;
                    otherId8 = currentMatch.FellowPlayers[7].SummonerId;
                    if (currentMatch.FellowPlayers[7].TeamId == 100)
                    {
                        btnName8.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName8.SetTextColor(Color.Red);
                    }

                    btnName9.Text = api.GetSummonerName(region, currentMatch.FellowPlayers[8].SummonerId).Name;
                    otherId9 = currentMatch.FellowPlayers[8].SummonerId;
                    if (currentMatch.FellowPlayers[8].TeamId == 100)
                    {
                        btnName9.SetTextColor(Color.Blue);
                    }
                    else
                    {
                        btnName9.SetTextColor(Color.Red);
                    }
                }
                catch (Exception)
                {
                    //only hide the "Other Players" buttons for bots
                    if (btnName1.Text == "Name")
                    {
                        btnName1.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName2.Text == "Name")
                    {
                        btnName2.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName3.Text == "Name")
                    {
                        btnName3.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName4.Text == "Name")
                    {
                        btnName4.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName5.Text == "Name")
                    {
                        btnName5.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName6.Text == "Name")
                    {
                        btnName6.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName7.Text == "Name")
                    {
                        btnName7.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName8.Text == "Name")
                    {
                        btnName8.Visibility = Android.Views.ViewStates.Gone;
                    }
                    if (btnName9.Text == "Name")
                    {
                        btnName9.Visibility = Android.Views.ViewStates.Gone;
                    }
                }
                
                txtSummonerName.Text = summonerName;
                

                //two exceptions to the naming rule in the static API
                string championName;
                if (currentMatch.ChampionId == 62)
                {
                    championName = "MonkeyKing";
                    txtChampName.Text = "Wukong";
                }
                else if(currentMatch.ChampionId == 31)
                {
                    championName = "Chogath";
                    txtChampName.Text = "Cho'Gath";
                }
                else
                {
                    championName = info.GetChampion(region, currentMatch.ChampionId).Name;
                    txtChampName.Text = championName;
                }
                
                try
                {
                    //remove spaces from names for url
                    championName = Regex.Replace(championName, @"\s+", "");
                    var championPic = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/champion/" + championName + ".png");
                    champPic.SetImageBitmap(championPic);
                }
                catch (Exception) { }

                var spells = info.GetSummonerSpells(region, RiotSharp.StaticDataEndpoint.SummonerSpellData.all);
                try
                {
                    var spell1ID = currentMatch.SummonerSpell1;
                    var spell1 = spells.SummonerSpells.First(s => s.Value.Id == spell1ID);
                    txtSpell1.Text = spell1.Value.Name;
                    var spellPic1 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/spell/" + spell1.Value.Image.Full);
                    imgSpell1.SetImageBitmap(spellPic1);
                }
                catch (Exception) { }

                try
                {
                    var spell2ID = currentMatch.SummonerSpell2;
                    var spell2 = spells.SummonerSpells.First(s => s.Value.Id == spell2ID);
                    txtSpell2.Text = spell2.Value.Name;
                    var spellPic2 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/spell/" + spell2.Value.Image.Full);
                    imgSpell2.SetImageBitmap(spellPic2);
                }
                catch (Exception) { }


                //trying each item indivually in case an item is sold before the game is over, default picture will stay
                try
                {
                    var item0Info = info.GetItem(region, currentMatch.Statistics.Item0);
                    txtItem0.Text = item0Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var item1Info = info.GetItem(region, currentMatch.Statistics.Item1);
                    txtItem1.Text = item1Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var item2Info = info.GetItem(region, currentMatch.Statistics.Item2);
                    txtItem2.Text = item2Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var item3Info = info.GetItem(region, currentMatch.Statistics.Item3);
                    txtItem3.Text = item3Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var item4Info = info.GetItem(region, currentMatch.Statistics.Item4);
                    txtItem4.Text = item4Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var item5Info = info.GetItem(region, currentMatch.Statistics.Item5);
                    txtItem5.Text = item5Info.Name;
                }
                catch (Exception) { }

                try
                {
                    var trinketInfo = info.GetItem(region, currentMatch.Statistics.Item6);
                    txtTrinket.Text = trinketInfo.Name;
                }
                catch (Exception) { }

                
                try
                {
                    var item0 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item0 + ".png");
                    item0Pic.SetImageBitmap(item0);
                }
                catch (Exception){ }

                try
                {
                    var item1 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item1 + ".png");
                    item1Pic.SetImageBitmap(item1);
                }
                catch(Exception) { }

                try
                {
                    var item2 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item2 + ".png");
                    item2Pic.SetImageBitmap(item2);
                }
                catch (Exception) { }

                try
                {
                    var item3 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item3 + ".png");
                    item3Pic.SetImageBitmap(item3);
                }
                catch (Exception){ }
                
                try
                {
                    var item4 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item4 + ".png");
                    item4Pic.SetImageBitmap(item4);
                }
                catch (Exception) { }
                
                try
                {
                    var item5 = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item5 + ".png");
                    item5Pic.SetImageBitmap(item5);
                }
                catch (Exception) { }
                
                try
                {
                    var Trinket = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/item/" + currentMatch.Statistics.Item6 + ".png");
                    itemTrinket.SetImageBitmap(Trinket);
                }
                catch (Exception) { }

                kills = currentMatch.Statistics.ChampionsKilled;
                deaths = currentMatch.Statistics.NumDeaths;
                assists = currentMatch.Statistics.Assists;
                KDAText.Text = "Kills/Deaths/Assists: " + kills.ToString() + "/" + deaths.ToString() + "/" + assists.ToString();
                double overallKDAValue = (kills + assists) / deaths;
                overallKDAValue = Math.Truncate(overallKDAValue * 100) / 100;
                overallKDA.Text = "Overall KDA: " + overallKDAValue;

                if (currentMatch.Statistics.Win == true && currentMatch.TeamId == 100)
                {
                    finalResult.Text = "Blue Team";
                    finalResult.SetTextColor(Color.Blue);
                    imgWinState.SetImageResource(Resource.Drawable.victoryIcon);
                }
                else if (currentMatch.Statistics.Win == true && currentMatch.TeamId == 200)
                {
                    finalResult.Text = "Red Team";
                    finalResult.SetTextColor(Color.Red);
                    imgWinState.SetImageResource(Resource.Drawable.victoryIcon);
                }
                else if(currentMatch.Statistics.Win == false && currentMatch.TeamId == 100)
                {
                    finalResult.Text = "Blue Team";
                    finalResult.SetTextColor(Color.Blue);
                    imgWinState.SetImageResource(Resource.Drawable.defeatIcon);
                }
                else if (currentMatch.Statistics.Win == false && currentMatch.TeamId == 200)
                {
                    finalResult.Text = "Red Team";
                    finalResult.SetTextColor(Color.Red);
                    imgWinState.SetImageResource(Resource.Drawable.defeatIcon);
                }
                else
                {
                    finalResult.Text = "Error!";
                }

                champLevel.Text = "Level: " + currentMatch.Statistics.Level.ToString();
                
            }
            catch (Exception) { }
        }

        //button handlers that essentially restart the activity with a new summoner's info from the game
        //the redundant toasts are there because sometimes it throws an error if the user doesn't have 10 games in their match history
        void btnName1Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId1).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId1).Name);
                        StartActivity(myIntent);
                    }
                }
                if(history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch(Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName2Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId2).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId2).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch(Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
            
        }
        void btnName3Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId3).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId3).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch(Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName4Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId4).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId4).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch(Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName5Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId5).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId5).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName6Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId6).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId6).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName7Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId7).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId7).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName8Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId8).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId8).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }
        void btnName9Click(object sender, EventArgs e)
        {
            try
            {
                List<Game> games = api.GetSummoner(region, otherId9).GetRecentGames();
                for (int i = 0; i < 9; i++)
                {
                    if (games[i].GameId == matchId)
                    {
                        history = true;
                        SerializeObject(games[i], "matchDetails.xml");
                        Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                        myIntent.PutExtra("summonerName", api.GetSummonerName(region, otherId9).Name);
                        StartActivity(myIntent);
                    }
                }
                if (history == false)
                {
                    Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
                }
            }
            catch (Exception)
            {
                Toast.MakeText(this, "Game no longer in summoner's match history", ToastLength.Long).Show();
            }
        }

        void btnBackClick(object sender, EventArgs e)
        {
            StartActivity(typeof(MainActivity));
        }

        public Game DeSerializeObject<Game>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return default(Game); }

            Game objectOut = default(Game);

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(fileName);
                string xmlString = xmlDocument.OuterXml;

                using (StringReader read = new StringReader(xmlString))
                {
                    Type outType = typeof(Game);

                    XmlSerializer serializer = new XmlSerializer(outType);
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        objectOut = (Game)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }
            }
            catch (Exception) { }
            return objectOut;
        }

        public void SerializeObject<Game>(Game serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                var doc = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                var path = System.IO.Path.Combine(doc, fileName);
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(path);
                    stream.Close();
                }
            }
            catch (Exception) { }
        }
        private Android.Graphics.Bitmap GetImageBitmapFromUrl(string url)
        {
            Android.Graphics.Bitmap imageBitmap = null;

            using (var webClient = new System.Net.WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }
    }
}