﻿
/*****************************************************************************/
/*                                                                           */
/* CMPS383Phase2                                                             */
/* Team One                                                                  */
/* Last Date updated: 02/09/2017                                             */
/* Class:  MainActivity                                                      */
/*                                                                           */
/*****************************************************************************/

using Android.App;
using Android.OS;
using RiotSharp;
using System;
using Android.Widget;
using Android.Content.PM;
using System.Collections.Generic;
using RiotSharp.GameEndpoint;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Android.Content;

namespace CMPS383Phase2
{//begin namespace
	
    [Activity(Label = "Phase 2", Icon = "@drawable/leagueicon",
		ScreenOrientation = ScreenOrientation.Portrait)]

    public class MainActivity : Activity
    {//begin class

		private EditText txtName;
		private Button btnSearch;
        private Button btnMatch1, btnMatch2, btnMatch3, btnMatch4, btnMatch5, btnMatch6, btnMatch7, btnMatch8, btnMatch9, btnMatch10;
        private TextView txtMatchID1, txtMatchID2, txtMatchID3, txtMatchID4, txtMatchID5, txtMatchID6, txtMatchID7, txtMatchID8, txtMatchID9, txtMatchID10;
		private ImageView imgUserIcon;
        //private long matchID1, matchID2, matchID3, matchID4, matchID5, matchID6, matchID7, matchID8, matchID9, matchID10;
        public static Game game1, game2, game3, game4, game5, game6, game7, game8, game9, game10;
        private List<Game> gameList;
        private string summonerName;
        private bool found = false;

        private TextView txtLevel, txtDetails, txtRevisionDate, txtMatch1, txtMatch2, txtMatch3, txtMatch4, txtMatch5, txtMatch6, txtMatch7, txtMatch8, txtMatch9, txtMatch10;
        private TextView txtMatchDate1, txtMatchDate2, txtMatchDate3, txtMatchDate4, txtMatchDate5, txtMatchDate6, txtMatchDate7, txtMatchDate8, txtMatchDate9, txtMatchDate10;
        private TextView txtWinLoss1, txtWinLoss2, txtWinLoss3, txtWinLoss4, txtWinLoss5, txtWinLoss6, txtWinLoss7, txtWinLoss8, txtWinLoss9, txtWinLoss10;
        private Space space1, space2, space3, space4;
        private ImageView imgMenuTop, imgMenuBottom;
  
        protected override void OnCreate(Bundle savedInstanceState)
        {//begin method
            base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.MatchHistory2);

            txtLevel = FindViewById<TextView>(Resource.Id.txtLevel);
            txtDetails = FindViewById<TextView>(Resource.Id.txtDetails);
            txtRevisionDate = FindViewById<TextView>(Resource.Id.txtRevisionDate);

            txtMatch1 = FindViewById<TextView>(Resource.Id.txtMatch1);
            txtMatch2 = FindViewById<TextView>(Resource.Id.txtMatch2);
            txtMatch3 = FindViewById<TextView>(Resource.Id.txtMatch3);
            txtMatch4 = FindViewById<TextView>(Resource.Id.txtMatch4);
            txtMatch5 = FindViewById<TextView>(Resource.Id.txtMatch5);
            txtMatch6 = FindViewById<TextView>(Resource.Id.txtMatch6);
            txtMatch7 = FindViewById<TextView>(Resource.Id.txtMatch7);
            txtMatch8 = FindViewById<TextView>(Resource.Id.txtMatch8);
            txtMatch9 = FindViewById<TextView>(Resource.Id.txtMatch9);
            txtMatch10 = FindViewById<TextView>(Resource.Id.txtMatch10);

            txtMatchDate1 = FindViewById<TextView>(Resource.Id.txtMatchDate1);
            txtMatchDate2 = FindViewById<TextView>(Resource.Id.txtMatchDate2);
            txtMatchDate3 = FindViewById<TextView>(Resource.Id.txtMatchDate3);
            txtMatchDate4 = FindViewById<TextView>(Resource.Id.txtMatchDate4);
            txtMatchDate5 = FindViewById<TextView>(Resource.Id.txtMatchDate5);
            txtMatchDate6 = FindViewById<TextView>(Resource.Id.txtMatchDate6);
            txtMatchDate7 = FindViewById<TextView>(Resource.Id.txtMatchDate7);
            txtMatchDate8 = FindViewById<TextView>(Resource.Id.txtMatchDate8);
            txtMatchDate9 = FindViewById<TextView>(Resource.Id.txtMatchDate9);
            txtMatchDate10 = FindViewById<TextView>(Resource.Id.txtMatchDate10);

            txtWinLoss1 = FindViewById<TextView>(Resource.Id.txtWinLoss1);
            txtWinLoss2 = FindViewById<TextView>(Resource.Id.txtWinLoss2);
            txtWinLoss3 = FindViewById<TextView>(Resource.Id.txtWinLoss3);
            txtWinLoss4 = FindViewById<TextView>(Resource.Id.txtWinLoss4);
            txtWinLoss5 = FindViewById<TextView>(Resource.Id.txtWinLoss5);
            txtWinLoss6 = FindViewById<TextView>(Resource.Id.txtWinLoss6);
            txtWinLoss7 = FindViewById<TextView>(Resource.Id.txtWinLoss7);
            txtWinLoss8 = FindViewById<TextView>(Resource.Id.txtWinLoss8);
            txtWinLoss9 = FindViewById<TextView>(Resource.Id.txtWinLoss9);
            txtWinLoss10 = FindViewById<TextView>(Resource.Id.txtWinLoss10);

            space1 = FindViewById<Space>(Resource.Id.space1);
            space2 = FindViewById<Space>(Resource.Id.space2);
            space3 = FindViewById<Space>(Resource.Id.space3);
            space4 = FindViewById<Space>(Resource.Id.space4);

            imgMenuTop = FindViewById<ImageView>(Resource.Id.imgMenuTop);
            imgMenuBottom = FindViewById<ImageView>(Resource.Id.imgMenuBottom);

            /*btnChampionInfo = FindViewById<Button>(Resource.Id.btnChampionInfo);
			txtLevel = FindViewById<TextView>(Resource.Id.txtLevel);
			txtID = FindViewById<TextView>(Resource.Id.txtID);
            btnChampionInfo.Click += btnChampionInfoClick;*/

            txtName = FindViewById<EditText>(Resource.Id.txtName);

            btnSearch = FindViewById<Button>(Resource.Id.btnSearch);
            btnSearch.Click += btnSearchClick;

            btnMatch1 = FindViewById<Button>(Resource.Id.btnMatch1);
            btnMatch1.Click += btnMatch1Click;

            btnMatch2 = FindViewById<Button>(Resource.Id.btnMatch2);
            btnMatch2.Click += btnMatch2Click;

            btnMatch3 = FindViewById<Button>(Resource.Id.btnMatch3);
            btnMatch3.Click += btnMatch3Click;

            btnMatch4 = FindViewById<Button>(Resource.Id.btnMatch4);
            btnMatch4.Click += btnMatch4Click;

            btnMatch5 = FindViewById<Button>(Resource.Id.btnMatch5);
            btnMatch5.Click += btnMatch5Click;

            btnMatch6 = FindViewById<Button>(Resource.Id.btnMatch6);
            btnMatch6.Click += btnMatch6Click;

            btnMatch7 = FindViewById<Button>(Resource.Id.btnMatch7);
            btnMatch7.Click += btnMatch7Click;

            btnMatch8 = FindViewById<Button>(Resource.Id.btnMatch8);
            btnMatch8.Click += btnMatch8Click;

            btnMatch9 = FindViewById<Button>(Resource.Id.btnMatch9);
            btnMatch9.Click += btnMatch9Click;

            btnMatch10 = FindViewById<Button>(Resource.Id.btnMatch10);
            btnMatch10.Click += btnMatch10Click;

            txtMatchID1 = FindViewById<TextView>(Resource.Id.txtMatchID1);
            txtMatchID2 = FindViewById<TextView>(Resource.Id.txtMatchID2);
            txtMatchID3 = FindViewById<TextView>(Resource.Id.txtMatchID3);
            txtMatchID4 = FindViewById<TextView>(Resource.Id.txtMatchID4);
            txtMatchID5 = FindViewById<TextView>(Resource.Id.txtMatchID5);
            txtMatchID6 = FindViewById<TextView>(Resource.Id.txtMatchID6);
            txtMatchID7 = FindViewById<TextView>(Resource.Id.txtMatchID7);
            txtMatchID8 = FindViewById<TextView>(Resource.Id.txtMatchID8);
            txtMatchID9 = FindViewById<TextView>(Resource.Id.txtMatchID9);
            txtMatchID10 = FindViewById<TextView>(Resource.Id.txtMatchID10);

			imgUserIcon = FindViewById<ImageView>(Resource.Id.imgUserIcon);

        }
        void btnSearchClick(object sender, EventArgs e)
		{
            imgUserIcon.SetImageResource(Resource.Drawable.leagueIcon);
            try
            {
                game1 = null;
                game2 = null;
                game3 = null;
                game4 = null;
                game5 = null;
                game6 = null;
                game7 = null;
                game8 = null;
                game9 = null;
                game10 = null;
            }
            catch(Exception) {}
            
            //reset text fields
            txtWinLoss1.Text = "Victory/Defeat";
            txtWinLoss2.Text = "Victory/Defeat";
            txtWinLoss3.Text = "Victory/Defeat";
            txtWinLoss4.Text = "Victory/Defeat";
            txtWinLoss5.Text = "Victory/Defeat";
            txtWinLoss6.Text = "Victory/Defeat";
            txtWinLoss7.Text = "Victory/Defeat";
            txtWinLoss8.Text = "Victory/Defeat";
            txtWinLoss9.Text = "Victory/Defeat";
            txtWinLoss10.Text = "Victory/Defeat";

            txtMatchDate1.Text = "Date:";
            txtMatchDate2.Text = "Date:";
            txtMatchDate3.Text = "Date:";
            txtMatchDate4.Text = "Date:";
            txtMatchDate5.Text = "Date:";
            txtMatchDate6.Text = "Date:";
            txtMatchDate7.Text = "Date:";
            txtMatchDate8.Text = "Date:";
            txtMatchDate9.Text = "Date:";
            txtMatchDate10.Text = "Date:";

            /*
            //hide the buttons and text in case a user hasn't played 10 games
            btnMatch1.Visibility = Android.Views.ViewStates.Gone;
            btnMatch2.Visibility = Android.Views.ViewStates.Gone;
            btnMatch3.Visibility = Android.Views.ViewStates.Gone;
            btnMatch4.Visibility = Android.Views.ViewStates.Gone;
            btnMatch5.Visibility = Android.Views.ViewStates.Gone;
            btnMatch6.Visibility = Android.Views.ViewStates.Gone;
            btnMatch7.Visibility = Android.Views.ViewStates.Gone;
            btnMatch8.Visibility = Android.Views.ViewStates.Gone;
            btnMatch9.Visibility = Android.Views.ViewStates.Gone;
            btnMatch10.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID1.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID2.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID3.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID4.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID5.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID6.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID7.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID8.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID9.Visibility = Android.Views.ViewStates.Gone;
            txtMatchID10.Visibility = Android.Views.ViewStates.Gone;
            */

            //(constrained) stats about summoners.
            var api = RiotApi.GetInstance("RGAPI-249041df-8508-449a-81b1-ae2e9f820ed1");
			var info = StaticRiotApi.GetInstance("RGAPI-249041df-8508-449a-81b1-ae2e9f820ed1");

			//(unconstrained) can be used to retrieve status information.  no api key needed.
			//var status = StatusRiotApi.GetInstance();
			var region = (Region)Enum.Parse(typeof(Region), "na");
            try
            {
                var temp = api.GetSummoner(region, txtName.Text);

                //this is just to make sure we don't show the wrong error message
                found = true;

                var summon = new SummonerDto(temp.Id, temp.Name, temp.ProfileIconId, temp.RevisionDate, temp.Level);
				var imageBitmap = GetImageBitmapFromUrl("http://ddragon.leagueoflegends.com/cdn/7.3.3/img/profileicon/" + temp.ProfileIconId + ".png");
				imgUserIcon.SetImageBitmap(imageBitmap);

                summonerName = temp.Name;
                txtLevel.Text = "Level: " + summon.getSummonerLevel();
                txtRevisionDate.Text = "Last Played: " + summon.getRevisionDate().ToString();

                gameList = temp.GetRecentGames();
                try
                {
                    game1 = gameList[0];
                    txtMatchDate1.Text = gameList[0].CreateDate.ToString();
                    if(gameList[0].Statistics.Win == true)
                    {
                        txtWinLoss1.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss1.Text = "Defeat";
                    }
                    /*
                    btnMatch1.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID1.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID1.Text = "Match ID: " + gameList[0].GameId.ToString();
                    matchID1 = gameList[0].GameId;
                    */

                    game2 = gameList[1];
                    txtMatchDate2.Text = gameList[1].CreateDate.ToString();
                    if (gameList[1].Statistics.Win == true)
                    {
                        txtWinLoss2.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss2.Text = "Defeat";
                    }
                    /*
                    btnMatch2.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID2.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID2.Text = "Match ID: " + gameList[1].GameId.ToString();
                    matchID2 = gameList[1].GameId;
                    */

                    game3 = gameList[2];
                    txtMatchDate3.Text = gameList[2].CreateDate.ToString();
                    if (gameList[2].Statistics.Win == true)
                    {
                        txtWinLoss3.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss3.Text = "Defeat";
                    }
                    /*
                    btnMatch3.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID3.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID3.Text = "Match ID: " + gameList[2].GameId.ToString();
                    matchID3 = gameList[2].GameId;
                    */

                    game4 = gameList[3];
                    txtMatchDate4.Text = gameList[3].CreateDate.ToString();
                    if (gameList[3].Statistics.Win == true)
                    {
                        txtWinLoss4.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss4.Text = "Defeat";
                    }
                    /*
                    btnMatch4.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID4.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID4.Text = "Match ID: " + gameList[3].GameId.ToString();
                    matchID4 = gameList[3].GameId;
                    */

                    game5 = gameList[4];
                    txtMatchDate5.Text = gameList[4].CreateDate.ToString();
                    if (gameList[4].Statistics.Win == true)
                    {
                        txtWinLoss5.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss5.Text = "Defeat";
                    }
                    /*
                    btnMatch5.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID5.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID5.Text = "Match ID: " + gameList[4].GameId.ToString();
                    matchID5 = gameList[4].GameId;
                    */

                    game6 = gameList[5];
                    txtMatchDate6.Text = gameList[5].CreateDate.ToString();
                    if (gameList[5].Statistics.Win == true)
                    {
                        txtWinLoss6.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss6.Text = "Defeat";
                    }
                    /*
                    btnMatch6.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID6.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID6.Text = "Match ID: " + gameList[5].GameId.ToString();
                    matchID6 = gameList[5].GameId;
                    */

                    game7 = gameList[6];
                    txtMatchDate7.Text = gameList[6].CreateDate.ToString();
                    if (gameList[6].Statistics.Win == true)
                    {
                        txtWinLoss7.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss7.Text = "Defeat";
                    }
                    /*
                    btnMatch7.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID7.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID7.Text = "Match ID: " + gameList[6].GameId.ToString();
                    matchID7 = gameList[6].GameId;
                    */

                    game8 = gameList[7];
                    txtMatchDate8.Text = gameList[7].CreateDate.ToString();
                    if (gameList[7].Statistics.Win == true)
                    {
                        txtWinLoss8.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss8.Text = "Defeat";
                    }
                    /*
                    btnMatch8.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID8.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID8.Text = "Match ID: " + gameList[7].GameId.ToString();
                    matchID8 = gameList[7].GameId;
                    */


                    game9 = gameList[8];
                    txtMatchDate9.Text = gameList[8].CreateDate.ToString();
                    if (gameList[8].Statistics.Win == true)
                    {
                        txtWinLoss9.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss9.Text = "Defeat";
                    }
                    /*
                    btnMatch9.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID9.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID9.Text = "Match ID: " + gameList[8].GameId.ToString();
                    matchID9 = gameList[8].GameId;
                    */

                    game10 = gameList[9];
                    txtMatchDate10.Text = gameList[9].CreateDate.ToString();
                    if (gameList[9].Statistics.Win == true)
                    {
                        txtWinLoss10.Text = "Victory";
                    }
                    else
                    {
                        txtWinLoss10.Text = "Defeat";
                    }
                    /*
                    btnMatch10.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID10.Visibility = Android.Views.ViewStates.Visible;
                    txtMatchID10.Text = "Match ID: " + gameList[9].GameId.ToString();
                    matchID10 = gameList[9].GameId;
                    */
                }
                catch (Exception) { }
            }
            catch(Exception)
            {
                if(found == false)
                {
                    Toast.MakeText(this, "Summoner not found!", ToastLength.Short).Show();
                }
            }
			

		}
        void btnMatch1Click(object sender, EventArgs e)
        {
            if(game1 != null)
            {
                SerializeObject(game1, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
            
        }
        void btnMatch2Click(object sender, EventArgs e)
        {
            if (game2 != null)
            {
                SerializeObject(game2, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch3Click(object sender, EventArgs e)
        {
            if (game3 != null)
            {
                SerializeObject(game3, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch4Click(object sender, EventArgs e)
        {
            if (game4 != null)
            {
                SerializeObject(game4, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch5Click(object sender, EventArgs e)
        {
            if (game5 != null)
            {
                SerializeObject(game5, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch6Click(object sender, EventArgs e)
        {
            if (game6 != null)
            {
                SerializeObject(game6, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch7Click(object sender, EventArgs e)
        {
            if (game7 != null)
            {
                SerializeObject(game7, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch8Click(object sender, EventArgs e)
        {
            if (game8 != null)
            {
                SerializeObject(game8, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
        }
        void btnMatch9Click(object sender, EventArgs e)
        {
            if (game9 != null)
            {
                SerializeObject(game9, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }
        void btnMatch10Click(object sender, EventArgs e)
        {
            if (game10 != null)
            {
                SerializeObject(game10, "matchDetails.xml");
                Intent myIntent = new Intent(this, typeof(MatchDetailsActivity));
                myIntent.PutExtra("summonerName", summonerName);
                StartActivity(myIntent);
            }
            else
            {
                Toast.MakeText(this, "Game not available!", ToastLength.Short).Show();
            }
        }

        public void SerializeObject<Game>(Game serializableObject, string fileName)
        {
            if (serializableObject == null) { return; }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer serializer = new XmlSerializer(serializableObject.GetType());
                var doc = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                var path = Path.Combine(doc, fileName);
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, serializableObject);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save(path);
                    stream.Close();
                }
            }
            catch (Exception) { }
        }

		private Android.Graphics.Bitmap GetImageBitmapFromUrl(string url)
		{
			Android.Graphics.Bitmap imageBitmap = null;

			using (var webClient = new System.Net.WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = Android.Graphics.BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}
    }
}

