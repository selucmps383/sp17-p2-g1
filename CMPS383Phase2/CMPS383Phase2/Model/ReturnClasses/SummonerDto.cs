﻿
/*****************************************************************************/
/*                                                                           */
/* CMPS383Phase2                                                             */
/* Team One                                                                  */
/* Last Date updated: 02/09/2017                                             */
/* Class:  SummonerDto                                                       */
/*                                                                           */
/*****************************************************************************/

using System;

namespace CMPS383Phase2
{//begin namespace
	
	public class SummonerDto
	{//begin class 

		private int profileIconId;
		private long id;
		private DateTime revisionDate;
		private long summonerLevel;
		private string name;

		//todo if we end up supporting different mobas, it would prolly be a good idea to convert this into a more
		//generic abstract class, and create different summoner subclasses for each moba.  this way, we could create
		//methods that can iterate through all summons, regardless of game.
		
		public SummonerDto(long newID, string newName, int newProfileIconID, DateTime newRevisionDate, long
		newSummonerLevel)
		{//begin constructor
			profileIconId = newProfileIconID;
			id = newID;
			revisionDate = newRevisionDate;
			summonerLevel = newSummonerLevel;
			name = newName;
		}//end constructor

		public int getProfileIconID()
		{//begin method
			return profileIconId;
		}//end method

		public void setProfileIconID(int newProfileIconID)
		{//begin method
			profileIconId = newProfileIconID;
		}//end method

		public long getID()
		{//begin method
			return id;
		}//end method

		public void setID(long newID)
		{//begin method
			id = newID;
		}//end method

		public DateTime getRevisionDate()
		{//begin method
			return revisionDate;
		}//end method

		public void setRevisionDate(DateTime newRevisionDate)
		{//begin method
			revisionDate = newRevisionDate;
		}//end method

		public long getSummonerLevel()
		{//begin method
			return summonerLevel;
		}//end method

		public void setSummonerLevel(long newSummonerLevel)
		{//begin method
			summonerLevel = newSummonerLevel;
		}//end method

		public string getName()
		{//begin method
			return name;
		}//end method

		public void setName(string newName)
		{//begin method
			name = newName;
		}//end method

	}//end class

}//end namespace
