using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace CMPS383Phase2
{
    [Activity(Label = "Phase 2", MainLauncher = true, Icon = "@drawable/leagueicon",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class GameSelectActivity : Activity
    {
        private Button btnLeague;
        private Button btnWarcraft;
        private TextView txtWelcome;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.GameSelect2);
           // SetContentView(Resource.Layout.GameSelect);

            //txtWelcome = FindViewById<TextView>(Resource.Id.txtWelcome);

            btnLeague = FindViewById<Button>(Resource.Id.btnLeague);
            btnLeague.Click += btnLeagueClick;

            btnWarcraft = FindViewById<Button>(Resource.Id.btnWarcraft);
            btnWarcraft.Click += btnWarcraftClick;
        }
        void btnLeagueClick(object sender, EventArgs e)
        {
            StartActivity(typeof(MainActivity));
        }
        void btnWarcraftClick(object sender, EventArgs e)
        {
            StartActivity(typeof(WOWActivity));
        }
    }
}